// Ionic Starter App

angular.module('starter', ['ionic', 'Appged'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppProjetoCtrl'
  })

  .state('app.pesquisar', {
    url: "/pesquisar",
    views: {
      'menuContent': {
        templateUrl: "templates/projeto/pesquisar.html"
      }
    }
  })
    .state('app.projetos', {
      url: "/projetos",
      views: {
        'menuContent': {
          templateUrl: "templates/projeto/projetos.html",
          controller: 'AppProjetoCtrl'
        }
      }
    })

  .state('app.tarefa', {
    url: "/projetos/:projetolistId",
    views: {
      'menuContent': {
        templateUrl: "templates/projeto/tarefas.html",
        controller: 'AppProjetoCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/projetos');
});
