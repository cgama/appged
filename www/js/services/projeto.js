appgedServices.factory('Projeto', function () {

    var STORAGE_ID = 'todo-ionic-projeto';
    return {
        get: function () {
            return JSON.parse(localStorage.getItem(STORAGE_ID) || '[]');
        },
        put: function (todos) {
            localStorage.setItem(STORAGE_ID, JSON.stringify(todos));
        }
    };

});