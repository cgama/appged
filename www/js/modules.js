'use strict';

/*
 * Módulo de servicos
 */
var appgedServices = angular.module('Appged.services', ['ngResource']);

/*
 * Controles
 * Módulos de controles por entidade  
 */
var projetoControllers = angular.module('Projeto.controllers', []);

// Módulo com todos os módulos de controle
var appgedControllers = angular.module('Appged.controllers', [
    'Projeto.controllers'
]);

// Módulo principal da aplicação
angular.module('Appged', [
    'Appged.services',
    'Appged.controllers'
]);