appgedControllers
        .controller('AppProjetoCtrl', ['$scope', '$ionicModal', '$timeout', 'Projeto',
            function ($scope, $ionicModal, $timeout, Projeto) {

                var projetos = $scope.projetos = Projeto.get();

                $scope.init = function () {
                    $scope.titulo = '';
                    $scope.editarProjeto = null;
                };


                $scope.addProjeto = function (titulo) {
                    
                    var novoProjeto = titulo.trim();
                    
                    if (!novoProjeto.length) {
                        return;
                    }
                    projetos.push({
                        title: novoProjeto
                    });
                    
                    Projeto.put(projetos);
                    $scope.projetos = Projeto.get();
                    $scope.fecharModalProjeto();
                };

                //Editando Projeto
                $scope.editProjeto = function (projeto) {
                    $scope.editedProjeto = projeto;
                };

                //Removendo Projeto
                $scope.removeProjeto = function (projeto) {
                    projetos.splice(projetos.indexOf(projeto), 1);
                    Projeto.put(projetos);
                    $scope.projetos = Projeto.get();
                };


                // Adicionar projeto
                $ionicModal.fromTemplateUrl('templates/projeto/adicionar.html', {
                    scope: $scope
                }).then(function (modal) {
                    $scope.modal = modal;
                });

                // Fechar Modal que adicona projeto
                $scope.fecharModalProjeto = function () {
                    $scope.modal.hide();
                };

                // Abrir Modal para adicionar projeto
                $scope.abrirModalProjeto = function () {
                    $scope.modal.show();
                };

            }])
        .controller('ProjetosListaCtrl', ['$scope', '$stateParams', '$ionicLoading', '$timeout', 'Projeto',
            function ($scope, $stateParams, $ionicLoading, $timeout, Projeto) {
//                
//                
//                $scope.projetos = [
//                        {title: 'Reggae', id: 1},
//                        {title: 'Chill', id: 2},
//                        {title: 'Dubstep', id: 3},
//                        {title: 'Indie', id: 4},
//                        {title: 'Rap', id: 5},
//                        {title: 'Cowbell', id: 6}
//                    ];
//                
//                // Configurando loading
//                $ionicLoading.show({
//                    content: 'Carregando ...',
//                    template: 'Carregando...',
//                    animation: 'fade-in',
//                    showBackdrop: true,
//                    maxWidth: 200,
//                    showDelay: 0
//                });
//
//                // Set a timeout to clear loader, however you would actually call the $ionicLoading.hide(); method whenever everything is ready or loaded.
//                $timeout(function () {
//                    $ionicLoading.hide();
//                    
////                    $scope.playlists = [
////                        {title: 'Reggae', id: 1},
////                        {title: 'Chill', id: 2},
////                        {title: 'Dubstep', id: 3},
////                        {title: 'Indie', id: 4},
////                        {title: 'Rap', id: 5},
////                        {title: 'Cowbell', id: 6}
////                    ];
//                }, 2000);
//

            }]);
